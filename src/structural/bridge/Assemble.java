package structural.bridge;

/**
 * concrete implementation2 of bridge pattern
 *
 */
public class Assemble implements Workshop {

	@Override
	public void work() {
		// TODO Auto-generated method stub
		System.out.println("Assembled");
	}

}

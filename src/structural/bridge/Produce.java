package structural.bridge;

/**
 * concrete implementation1 of bridge pattern
 *
 */
public class Produce implements Workshop {

	@Override
	public void work() {
		// TODO Auto-generated method stub
		System.out.println("Produced");
	}

}

package structural.bridge;

/**
 * Refine abstraction1 in brdige pattern
 *
 */
public class Car extends Vehicle {

	public Car(Workshop workshop1, Workshop workshop2){
		super(workshop1, workshop2);
	}
	
	@Override
	public void manufature() {
		// TODO Auto-generated method stub
		System.out.println("Car");
		workshop1.work();
		workshop2.work();
	}

}

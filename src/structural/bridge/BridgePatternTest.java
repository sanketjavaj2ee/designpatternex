package structural.bridge;

public class BridgePatternTest {

	public static void main(String[] args) {
		Vehicle vehicle1 = new Car(new Produce(), new Assemble());
		vehicle1.manufature();
		Vehicle vehicle2 = new Bike(new Produce(), new Assemble());
		vehicle2.manufature();
	}
}

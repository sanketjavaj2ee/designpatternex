package structural.bridge;

/**
 * Refine abstraction2 in brdige pattern
 *
 */
public class Bike extends Vehicle {

	public Bike(Workshop workshop1, Workshop workshop2){
		super(workshop1, workshop2);
	}
	
	@Override
	public void manufature() {
		// TODO Auto-generated method stub
		System.out.println("Bike");
		workshop1.work();
		workshop2.work();
	}

}

package structural.bridge;

/**
 * Implementor of bridge pattern.
 *
 */
public interface Workshop {
	
	public void work();
}

package structural.adapter;

/**
 * Consider that we have a third party library that provides print string 
 * functionality through PrintString class.This is our Adaptee
 *
 */
public class PrintString {  

	public void print(String s)  
	{  
		System.out.println(s);  
	}  
}  

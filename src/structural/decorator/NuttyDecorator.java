package structural.decorator;

public class NuttyDecorator extends IcecreamDecorator {

	public NuttyDecorator(Icecream specialIcecream){
		super(specialIcecream);
	}
	
	public String makeIcecream(){
		return specialIcecream.makeIcecream() + addNutts();
	}
	
	public String addNutts(){
		return " + cruncy nuts";
	}
}

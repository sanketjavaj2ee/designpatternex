package structural.decorator;

public interface Icecream {

	public String makeIcecream();
}

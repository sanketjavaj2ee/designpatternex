package structural.proxy;

public interface IFolder {

	public void performOperations();
}

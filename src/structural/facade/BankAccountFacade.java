package structural.facade;

public class BankAccountFacade {

	private int accountNumber;
	private int securityCode;

	private AccountNumberCheck accntChecker;
	private SecurityCodeCheck codeChecker;
	private FundsCheck fundChecker;

	private WelcomeToBank bankWelcome;

	public BankAccountFacade(int newAccountNumber, int newSecurityCode){
		accountNumber = newAccountNumber;
		securityCode = newSecurityCode;

		bankWelcome = new WelcomeToBank();
		accntChecker = new AccountNumberCheck();
		codeChecker = new SecurityCodeCheck();
		fundChecker = new FundsCheck();

	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public int getSecurityCode() {
		return securityCode;
	}

	public void withdrawCash(double cashToWithdraw){

		if(accntChecker.isAccountActive(getAccountNumber()) &&
				codeChecker.isCodeCorrect(getSecurityCode()) &&
				fundChecker.haveEnoughMoney(cashToWithdraw)) {

			System.out.println("Transaction Complete\n");
		}else {
			System.out.println("Transaction Failed\n");
		}
	}
	
	public  void depositeCash(double cashToDeposite){
		
		if(accntChecker.isAccountActive(getAccountNumber()) &&
				codeChecker.isCodeCorrect(getSecurityCode())){
			fundChecker.makeDeposite(cashToDeposite);
			System.out.println("Transaction Complete\n");
		}else {
			System.out.println("Transaction Failed\n");
		}
	}
}

package structural.facade;

public class FundsCheck {

	private double cashInAccount = 1000.00;
	
	public double getCashInAccount(){
		return cashInAccount;
	}
	
	public void decreaseCashInAccount(double cashWithdrawn){
		cashInAccount -= cashWithdrawn;
	}
	
	public void increaseCashInAccount(double cashDeposited){
		cashInAccount += cashDeposited;
	}
	
	public boolean haveEnoughMoney(double cashToWithdraw){
		if(cashToWithdraw > getCashInAccount()){
			System.out.println("Error: You dont have enough money");
			System.out.println("Current balance in the account: " +getCashInAccount());
			return false;
		}else{
			decreaseCashInAccount(cashToWithdraw);
			System.out.println("Withdrawn complete: Current Balance is: " +getCashInAccount());
			return true;
		}
	}
	
	public void makeDeposite(double cashToDeposit){
		increaseCashInAccount(cashToDeposit);
		System.out.println("Deposit is coomplete: Current balance is: " +getCashInAccount());
	}
}

package uml.aggregation;

public class Person {

	private Address currentAddress;
	private String name;
	
	public Person(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCurrentAddress(Address address){
		this.currentAddress = address;
	}
	
	public Address getCurrentAddress(){
		return currentAddress;
	}
}

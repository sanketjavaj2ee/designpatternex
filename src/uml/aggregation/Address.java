package uml.aggregation;

public class Address {

	public String addressDetails;
	
	public Address(String address){
		addressDetails = address;
	}
	
	public String getAddressDetails(){
		return addressDetails;
	}
}

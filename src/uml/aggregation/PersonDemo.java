package uml.aggregation;

public class PersonDemo {
	public static void main(String[] args) {
		Person person = new Person("Biswa");
		person.setCurrentAddress(new Address("Bangalore"));
		String address = person.getCurrentAddress().getAddressDetails();
		System.out.println("The current address of the person " + person.getName() + " is: "  + address);
	}
}

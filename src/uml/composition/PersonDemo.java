package uml.composition;

public class PersonDemo {

	public static void main(String[] args) {
		Person person = new Person("Biswaranjan", "Ray");
		String firstName = person.getName().getFirstName();
		String lastName = person.getName().getLastName();
		
		System.out.println("Person's First Name is : " + firstName 
				+ " and Last Name is : " +lastName);
	}
}

package uml.composition;

public class Person {

	private final Name name;
	
	public Person(String fName, String lName){
		name = new Name(fName, lName);
	}
	
	public Name getName(){
		return name;
	}
}

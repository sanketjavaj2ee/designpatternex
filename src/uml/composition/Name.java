package uml.composition;

public class Name {

	private String firstName;
	private String lastName;
	
	public Name(String fName, String lName){
		firstName = fName;
		lastName = lName;
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
}

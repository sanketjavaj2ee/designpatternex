/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package creational.abstractactory1;

/**
 *
 * @author mitu
 */
public class CanadaFinancialToolsFactory extends FinacialToolsFactory {
		public TaxProcessor createTaxProcessor() {
			return new CanadaTaxProcessor();
		}
		public ShipFeeProcessor createShipFeeProcessor() {
			return new CanadaShipFeeProcessor();
			}
	}

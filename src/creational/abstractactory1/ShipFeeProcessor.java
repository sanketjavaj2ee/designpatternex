/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package creational.abstractactory1;

/**
 *
 * @author mitu
 */
// Products
	public abstract class ShipFeeProcessor {
		abstract void calculateShipFee(Order order);
	}
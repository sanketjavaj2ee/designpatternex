/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package creational.abstractactory1;

/**
 *
 * @author mitu
 */
public class Application {

    public static void main(String[] args) {
        // .....
        String countryCode = "EU";
        Customer customer = new Customer();
        Order order = new Order();
        OrderProcessor orderProcessor = null;
        FinacialToolsFactory factory = null;

        if (countryCode.equals("EU")) {
            factory = new EuropeFinancialToolsFactory();
        } else if (countryCode.equals("CA")) {
            factory = new CanadaFinancialToolsFactory();
        }
        orderProcessor = new OrderProcessor(factory);
        orderProcessor.processOrder(order);
    }
}


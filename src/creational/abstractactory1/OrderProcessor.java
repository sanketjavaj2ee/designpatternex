/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package creational.abstractactory1;

/**
 *
 * @author mitu
 */
// Client
public class OrderProcessor {

    private TaxProcessor taxProcessor;
    private ShipFeeProcessor shipFeeProcessor;

    public OrderProcessor(FinacialToolsFactory factory) {
        taxProcessor = factory.createTaxProcessor();
        shipFeeProcessor = factory.createShipFeeProcessor();
    }

    public void processOrder(Order order) {
        // ....
        taxProcessor.calculateTaxes(order);
        shipFeeProcessor.calculateShipFee(order);
    // ....
    }
}
// Integration with the overall application


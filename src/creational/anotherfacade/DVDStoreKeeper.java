package creational.anotherfacade;

public class DVDStoreKeeper {

	public DVDs getDvds(String dvdType){
		
		if(dvdType.equals("Movies")){
			MoviesDVDs movieDvds = new MoviesDVDs("The Avengers");
			return movieDvds;
		}else if(dvdType.equals("Songs")){
			SongsDVDs songsDvds = new SongsDVDs("Linkin Park");
			return songsDvds;
		}else {
			FitnessDVDs fitnessDvds = new FitnessDVDs("Greg Plitt Fitness Matra");
			return fitnessDvds;
		}
	}
}

package creational.anotherfacade;

public class DVDs {

	private String name;
	
	public DVDs(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}

package creational.anotherfacade;

public class MoviesDVDs extends DVDs {

	private String name;
	
	public MoviesDVDs(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

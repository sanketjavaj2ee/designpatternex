package creational.anotherfacade;

public class FitnessDVDs extends DVDs {

	private String name;
	
	public FitnessDVDs(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}

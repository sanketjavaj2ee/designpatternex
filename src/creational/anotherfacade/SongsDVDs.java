package creational.anotherfacade;

public class SongsDVDs extends DVDs {

	private String name;
	
	public SongsDVDs(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

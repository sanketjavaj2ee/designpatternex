package creational.anotherfacade;

public class Customer {

	public static void main(String[] args) {
		DVDStoreKeeper keeper = new DVDStoreKeeper();
		DVDs dvds = keeper.getDvds("Fitness");
		System.out.println("The dvd name is : " + dvds.getName());
	}
}

package creational.singleton2;

public class SingletonClass {
	private SingletonClass() {
		// no code req'd
	}

	public static SingletonClass getSingletonObject() {
		if (ref == null)
			// it's ok, we can call this constructor
			ref = new SingletonClass();
		return ref;
	}

	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
		// that'll teach 'em
	}

	private static SingletonClass ref;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SingletonClass class1 = new SingletonClass();
		SingletonClass class2 = new SingletonClass();
		SingletonClass.getSingletonObject();

		System.out.println(class1);
		System.out.println(class2);
		System.out.println(SingletonClass.getSingletonObject());
		System.out.println(SingletonClass.getSingletonObject());
		System.out.println(SingletonClass.getSingletonObject());
		System.out.println(SingletonClass.getSingletonObject());

	}

}

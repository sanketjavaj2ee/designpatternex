package creational.prototype;

class Bike implements Cloneable {
	
	private int gears;
	private String bikeType;
	private String model;
	public Bike() {
		bikeType = "StandardBike";
		model = "RX125";
		gears = 4;
	}

	public Object clone() {
		try{
			return super.clone();
		}catch(CloneNotSupportedException e){
			return null;
		}
		//return new Bike();
	}

	public void makeSuperBike() {
		bikeType = "SuperBike";
		model = "R15";
		gears = 6;
	}
	public String getModel(){
		return model;
	}
	public int getGears() {
		return gears;
	}

	public String getBikeType() {
		return bikeType;
	}
}

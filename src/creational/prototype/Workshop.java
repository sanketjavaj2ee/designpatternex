package creational.prototype;

public class Workshop {
	
	public Bike makeR15(Bike basicBike) {
		basicBike.makeSuperBike();
		return basicBike;
	}
	
	public static void main(String args[]){
		Bike bike = new Bike();
		Bike cloneBike = (Bike) bike.clone();
	
		Workshop workShop = new Workshop();
		Bike superBike = workShop.makeR15(cloneBike);
		System.out.println("Bike Model: "+superBike.getModel());
		System.out.println("Bike Type: "+superBike.getBikeType());
		System.out.println("Bike Gears:  "+superBike.getGears());

		
	}
}

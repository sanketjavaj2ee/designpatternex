package creational.factory;

public interface Car {

	void getMileage();
	void getCost();

}

package creational.factory;

public class Honda implements Car {

	@Override
	public void getMileage() {
		// TODO Auto-generated method stub
		System.out.println("Honda shows a mileage of 12/14 in city/highway");
	}

	@Override
	public void getCost() {
		// TODO Auto-generated method stub
		System.out.println("Honda's average cost is 9lakh ruppess");
	}

}

package creational.factory;

public class CarFactory {
	
	public Car getCar(String brand){
		if(brand.equals("Audi"))	
			return new Audi();
		else if(brand.equals("Ford"))
			return new Ford();
		else if(brand.equals("Honda"))
			return new Honda();
		else if(brand.equals("Maruti"))
			return new Maruti();
		else 
			return null;
	}
}

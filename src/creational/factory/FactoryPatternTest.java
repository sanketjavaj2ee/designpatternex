package creational.factory;

public class FactoryPatternTest {

	public static void main(String[] args) {
		CarFactory factory = new CarFactory();
		Car car = factory.getCar("Honda");
		car.getMileage();
		car.getCost();
		System.out.println("===============================");
		car = factory.getCar("Audi");
		car.getMileage();
		car.getCost();
		System.out.println("===============================");
		car = factory.getCar("Ford");
		car.getMileage();
		car.getCost();
		System.out.println("===============================");
		car = factory.getCar("Maruti");
		car.getMileage();
		car.getCost();
	}
}

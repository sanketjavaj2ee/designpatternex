package creational.builder;

public interface GuitarBuilder {

	public void fitStrings();
	public void fitKeys();
	public void fitPickUps();
	public Guitar getGuitar();
}

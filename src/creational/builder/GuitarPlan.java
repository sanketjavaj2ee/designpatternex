package creational.builder;

public interface GuitarPlan {

	public void setStrings(String strings);
	
	public void setKeys(String keys);
	
	public void setPickUps(String pickUps);
}

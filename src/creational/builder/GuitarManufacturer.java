package creational.builder;

public class GuitarManufacturer {

	private GuitarBuilder guitarBuilder;
	
	public GuitarManufacturer(GuitarBuilder guitarBuilder){
		this.guitarBuilder = guitarBuilder;
	}
	
	public Guitar getGuitar(){
		return this.guitarBuilder.getGuitar();
	}
	
	public void manufactureGuitar(){
		this.guitarBuilder.fitKeys();
		this.guitarBuilder.fitStrings();
		this.guitarBuilder.fitPickUps();
	}
}

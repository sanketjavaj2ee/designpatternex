package creational.builder;

public class Guitar implements GuitarPlan {

	private String strings;
	private String keys;
	private String pickUps;
	
	@Override
	public void setStrings(String strings) {
		// TODO Auto-generated method stub
		this.strings = strings;
	}

	@Override
	public void setKeys(String keys) {
		// TODO Auto-generated method stub
		this.keys = keys;
	}

	@Override
	public void setPickUps(String pickUps) {
		// TODO Auto-generated method stub
		this.pickUps = pickUps;
	}
	
	public String getStrings() {
		return strings;
	}

	public String getKeys() {
		return keys;
	}

	public String getPickUps() {
		return pickUps;
	}

}

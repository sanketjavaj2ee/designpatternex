package creational.builder;

public class SemiAcousticGuitarBuilder implements GuitarBuilder {

	private Guitar guitar;
	
	public SemiAcousticGuitarBuilder(){
		this.guitar = new Guitar();
	}
	
	@Override
	public void fitStrings() {
		// TODO Auto-generated method stub
		this.guitar.setStrings("nylon strings");
	}

	@Override
	public void fitKeys() {
		// TODO Auto-generated method stub
		this.guitar.setKeys("normal keys");
	}

	@Override
	public void fitPickUps() {
		// TODO Auto-generated method stub
		this.guitar.setPickUps("normal pickups");
	}
	
	@Override
	public Guitar getGuitar(){
		return this.guitar;
	}

}

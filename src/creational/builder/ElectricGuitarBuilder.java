package creational.builder;

public class ElectricGuitarBuilder implements GuitarBuilder {

	private Guitar guitar;
	
	public ElectricGuitarBuilder(){
		this.guitar = new Guitar();
	}
	@Override
	public void fitStrings() {
		// TODO Auto-generated method stub
		this.guitar.setStrings("Steel Strings");
	}

	@Override
	public void fitKeys() {
		// TODO Auto-generated method stub
		this.guitar.setKeys("Steel Keys");
	}

	@Override
	public void fitPickUps() {
		// TODO Auto-generated method stub
		this.guitar.setPickUps("fishman pickups");
	}

	@Override
	public Guitar getGuitar() {
		// TODO Auto-generated method stub
		return this.guitar;
	}
}

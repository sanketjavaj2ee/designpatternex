package creational.builder;

public class BuilderPatternTest {

	public static void main(String[] args) {
		GuitarBuilder builder = new ElectricGuitarBuilder();
		GuitarManufacturer manufacturer = new GuitarManufacturer(builder);
		manufacturer.manufactureGuitar();
		Guitar guitar = manufacturer.getGuitar();
		System.out.println(guitar.getKeys());
		System.out.println(guitar.getStrings());
		System.out.println(guitar.getPickUps());

	}
}

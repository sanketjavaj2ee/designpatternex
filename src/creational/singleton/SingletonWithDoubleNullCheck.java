package creational.singleton;

/**
 * Creating singleton using double null check. This is also called lazy loading.
 *
 */
public class SingletonWithDoubleNullCheck {

	//If we do not make _instance variable volatile then Thread
	//which is creating instance of Singleton is not able to communicate other thread,
	//that instance has been created until it comes out of the Singleton block, so if
	//Thread A is creating Singleton instance and just after creation lost the CPU, 
	//all other thread will not be able to see value of _instance as not null and 
	//they will believe its still null.

	
	private static volatile SingletonWithDoubleNullCheck instance;
	
	private SingletonWithDoubleNullCheck(){
		
	}
	
	public static SingletonWithDoubleNullCheck getInstance(){
		if(instance == null){
			synchronized (SingletonWithDoubleNullCheck.class) {
				if(instance == null)
					instance = new SingletonWithDoubleNullCheck();
			}
		}
		return instance;
	}
}

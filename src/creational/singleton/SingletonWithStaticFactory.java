package creational.singleton;

/**
 * Creating singleton using static factory method. its also called early loading.
 *
 */
public class SingletonWithStaticFactory {

	private static final SingletonWithStaticFactory instance = 
			new SingletonWithStaticFactory();
	
	private SingletonWithStaticFactory(){
		
	}
	
	public static SingletonWithStaticFactory getInstance(){
		return instance;
	}
}

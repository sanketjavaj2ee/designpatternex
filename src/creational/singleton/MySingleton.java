package creational.singleton;

public class MySingleton {

	private static MySingleton singleton;
	
	private MySingleton(){
		
	}
	
	//check if instance is null then create and return else just returns the instance.
	public static MySingleton getSingleInstance(){
		if(singleton == null)
			singleton = new MySingleton();
		return singleton;
	}
	
	public Object clone() throws CloneNotSupportedException{
		throw new CloneNotSupportedException();
	}
}

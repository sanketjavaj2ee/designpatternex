package creational.singleton;

import java.lang.reflect.Constructor;

public class BreakMySingleton {

	public static void main(String[] args) {
		String A = "Test";
		String B = "Test";
		B = B.toUpperCase();
		
		//B = A;
		System.out.println("A " + A + "B "+B);
//		Constructor<MySingleton> pvtConstructor  = Class.forName(
//				"com.java.designpatterns.singleton").getDeclaredConstructors()[0];
	}
}

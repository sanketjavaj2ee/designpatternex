package creational.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class SingletonEnumTest {

	public static void main(String[] args) 
			throws ClassNotFoundException, IllegalArgumentException, SecurityException,
			InstantiationException, IllegalAccessException, InvocationTargetException{
		Constructor c = Class.forName("com.java.designpatterns.singleton.SingletonEnum").getDeclaredConstructors()[0];
		c.setAccessible(true);
		SingletonEnum object = (SingletonEnum) c.newInstance(null);
	}
	
	//Hence, we can conclude that best way to implement a Singleton pattern is by using Enum.
	//It is simple, straightforward and unbreakable.
}

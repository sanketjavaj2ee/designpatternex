package creational.singleton;

/**
 * That best way to implement a Singleton pattern is by using Enum.
 * It is simple, straightforward and unbreakable
 *
 */
public enum SingletonEnum {

	UNIQUE_INSTANCE;
}

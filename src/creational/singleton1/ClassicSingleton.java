/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package creational.singleton1;

/* The singleton implemented in Example 1 is easy to understand. The
 ClassicSingleton class maintains a static reference to the lone singleton
 instance and returns that reference from the static getInstance() method.

There are several interesting points concerning the ClassicSingleton class.
 First, ClassicSingleton employs a technique known as lazy instantiation to
 create the singleton; as a result, the singleton instance is not created until
 the getInstance() method is called for the first time. This technique ensures
 that singleton instances are created only when needed.*/
public class ClassicSingleton {
   public static int instanceCount = 0;
   private static ClassicSingleton instance = null;
   protected ClassicSingleton() {
      // Exists only to defeat instantiation.      
   }
   public static ClassicSingleton getInstance() {
      if(instance == null) {
         instance = new ClassicSingleton();
         instanceCount++;
      }
      return instance;
   }
}


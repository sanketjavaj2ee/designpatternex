/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package creational.singleton1;


public class ClassicSingletonExt extends ClassicSingleton {

    static int instanceCount1 = 0;
    public ClassicSingletonExt() {
        //super();
        ClassicSingleton.getInstance();
        instanceCount1++;
        
    }

    public static void main(String s[]){
        ClassicSingletonExt instance1 = new ClassicSingletonExt();
        System.out.println(ClassicSingleton.instanceCount + ": this class :"+instance1+":"+instanceCount1 );
        ClassicSingletonExt instance2 = new ClassicSingletonExt();
        System.out.println(ClassicSingleton.instanceCount + ": this class :"+instance2+":"+instanceCount1 );

    }
}


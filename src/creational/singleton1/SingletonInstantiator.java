/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package creational.singleton1;

/**
 notice that ClassicSingleton implements a protected constructor so
 clients cannot instantiate ClassicSingleton instances; however, you may be
 surprised to discover that the following code is perfectly legal:
 */
public class SingletonInstantiator {
  public SingletonInstantiator() {
   ClassicSingleton instance = ClassicSingleton.getInstance();
   System.out.println(ClassicSingleton.instanceCount);
   ClassicSingleton anotherInstance = new ClassicSingleton();
   System.out.println(ClassicSingleton.instanceCount);
   
  }
  public static void main(String s[]){
      new SingletonInstantiator();
  }
}
/*

How can the class in the preceding code fragment—which does not extend
 ClassicSingleton—create a ClassicSingleton instance if the ClassicSingleton
 constructor is protected? The answer is that protected constructors can be
 called by subclasses and by other classes in the same package. Because
 ClassicSingleton and SingletonInstantiator are in the same package
 (the default package), SingletonInstantiator() methods can create
 ClassicSingleton instances.
 * //sanket:but returns same instance,so no probs
 This dilemma has two solutions:
 You can make the ClassicSingleton constructor private so that only
 ClassicSingleton() methods call it; however, that means ClassicSingleton
 cannot be subclassed.
 Sometimes, that is a desirable solution; if so, it's
 a good idea to declare your singleton class final, which makes that intention
 explicit and allows the compiler to apply performance optimizations. The other
 solution is to put your singleton class in an explicit package, so classes in
 other packages (including the default package) cannot instantiate singleton instances.
 * */
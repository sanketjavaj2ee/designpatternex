package creational.abstractfactory;

public class AbstractFactoryTest {

	public static void main(String[] args) {
		CarBuyer car = new CarBuyer();
		AbstractCarFactory carFactory = null;
		//how  to fetch the value of current car factory.
		if(true){
			carFactory = new FordCarFactory();
		}else{
			carFactory = new HondaCarFactory();
		}
		car.buyCar(carFactory);
	}
}

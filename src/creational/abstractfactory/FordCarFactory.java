package creational.abstractfactory;

//ConcreteFactory1
public class FordCarFactory implements AbstractCarFactory {

	@Override
	public Car manufactureCar() {
		// TODO Auto-generated method stub
		Ford ford = new Ford();
		return ford;
	}

}

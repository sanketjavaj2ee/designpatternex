package creational.abstractfactory;

//AbstractFactory
public interface AbstractCarFactory {

	public Car manufactureCar();
}

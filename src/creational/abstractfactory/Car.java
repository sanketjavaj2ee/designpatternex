package creational.abstractfactory;

//The Abstract Product
public interface Car {

	public void setModel(String name);
	public void cost();
}

package creational.abstractfactory;

//ConcreteFactory2
public class HondaCarFactory implements AbstractCarFactory {

	@Override
	public Car manufactureCar() {
		// TODO Auto-generated method stub
		Honda honda = new Honda();
		return honda;
	}

}

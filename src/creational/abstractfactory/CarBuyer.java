package creational.abstractfactory;

public class CarBuyer {

	public void buyCar(AbstractCarFactory carFactory){
		Car car = carFactory.manufactureCar();
		car.setModel("New Car");
	}
}
